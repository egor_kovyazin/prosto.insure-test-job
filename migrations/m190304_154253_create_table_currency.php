<?php

use yii\db\Migration;

class m190304_154253_create_table_currency extends Migration
{
    public function up()
    {
        $this->createTable('currency', [
            'id'        => $this->primaryKey(),
            'name'      => $this->string()->notNull(),
            'rate'      => $this->double()->notNull(),
            'char_code' => $this->string(3)->notNull()->unique(),
        ]);
    }

    public function down()
    {
         $this->dropTable('currency');
    }
}
