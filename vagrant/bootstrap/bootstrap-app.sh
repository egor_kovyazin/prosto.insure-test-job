#!/usr/bin/env bash


##### Configure #####

# Create directories
if [ ! -d /home/vagrant/logs ]; then
  mkdir /home/vagrant/logs
  chown vagrant:vagrant /home/vagrant/logs
fi

# Replace apache configuration file
if [ -f /etc/apache2/sites-enabled/000-default.conf ]; then
  rm /etc/apache2/sites-enabled/000-default.conf
fi
ln -sfn /vagrant/bootstrap/config/apache.conf /etc/apache2/sites-enabled/app.conf
service apache2 restart

# Replace apache User and Group
sed -i 's/export APACHE_RUN_USER=.*/export APACHE_RUN_USER=vagrant/g' /etc/apache2/envvars
sed -i 's/export APACHE_RUN_GROUP=.*/export APACHE_RUN_GROUP=vagrant/g' /etc/apache2/envvars
service apache2 restart

# Update dependances
#sudo -u vagrant composer global require "fxp/composer-asset-plugin:~1.0.0"
sudo -u vagrant composer install -d /home/vagrant/app

# Init the database with rudimentary init data
cd /home/vagrant/app
php yii migrate --interactive=0

# Changing mysql access
sed -i 's/bind-address\(.*\)=.*/bind-address\1= 0.0.0.0/g' /etc/mysql/mysql.conf.d/mysqld.cnf
service mysql restart