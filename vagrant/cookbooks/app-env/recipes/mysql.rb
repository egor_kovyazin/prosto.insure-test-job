# Set root password
bash "init-mysql-root-password" do
  user "root"
  group "root"
  cwd "/tmp"
  code <<-EOH
    echo mysql-server-5.7 mysql-server/root_password password #{node["mysql"]["root_password"]} | debconf-set-selections
    echo mysql-server-5.7 mysql-server/root_password_again password #{node["mysql"]["root_password"]} | debconf-set-selections
  EOH
end

package "mysql-server-5.7" do
  action :install
  options "--force-yes"
end
