package "php7.2"
package "php7.2-curl"
package "php7.2-mysql"
package "php7.2-sqlite3"
package "php7.2-gd"
package "php-memcache"
package "php7.2-pgsql"
package "php7.2-intl"
package "php-imagick"
package "phpunit"

bash "add-mod-rewrite" do
  user "root"
  code <<-EOH
    a2enmod rewrite
    service apache2 restart
  EOH
end

