bash "install-composer" do
  not_if {File.exists?('/usr/local/bin/composer')}
  user "root"
  cwd "/tmp"
  code <<-EOH
    curl -sS https://getcomposer.org/installer | php
    mv composer.phar /usr/local/bin/composer
  EOH
end
