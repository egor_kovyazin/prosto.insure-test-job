Test project for Prosto.Insure on Yii 2
============================

DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
        CurrencyRatesController.php  command for updating currency rates in the table "currency"
      config/             contains application configurations
      controllers/        contains Web controller classes
      models/             contains model classes
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
        api/
            CurrencyTest.php  demonstrates the work of api "currency"
      vendor/             contains dependent 3rd-party packages
      web/                contains the entry script and Web resources



REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.4.0.


RUN UNDER VAGRANT
-------------

### Requirements for developing

    * VirtualBox >= 5.2.10 and Extension Pack
    * Vagrant >= 2.1.18
    * Account on https://atlas.hashicorp.com


### Quick start

    git clone git@bitbucket.org:egor_kovyazin/prosto.insure-test-job.git app && cd app \
    && git submodule sync && git submodule init && git submodule update \
    && cd vagrant \
    && vagrant plugin install vagrant-omnibus && vagrant plugin install vagrant-vbguest && vagrant up
    
    echo -e '\n# Prosto.Insure test project\n171.15.34.10 yii-test.local' >> /etc/hosts


When the vagrant environment is up, you'll be able to access http://yii-test.local in your browser
or 171.15.34.10:3306 in your MySQL client (if you don't use Sequel Pro yet, you definitely should).


TASK FOR TEST
-------------


Необходимо реализовать сервис с следующим функционалом с использованием фреймворка Yii2.

  1. В базе данных должна быть таблица currency c колонками:
    * id - первичный ключ
    * name - название валюты
    * rate - курс валюты к рублю
  1. Должна быть консольная команда для обновления данных в таблице currency. Данные по курсам валют можно взять отсюда: http://www.cbr.ru/scripts/XML_daily.asp
  1. Реализовать 2 REST API метода:
    * GET /currencies - должен возвращать список курсов валют с возможность пагинации
    * GET /currency/<id> - должен возвращать курс валюты для переданного id

API должно быть закрыто bearer авторизацией.
