<?php

return [
    'adminEmail'             => 'admin@prosto.insure.com',
    'currencyRatesSourceUrl' => 'http://www.cbr.ru/scripts/XML_daily.asp',
];
