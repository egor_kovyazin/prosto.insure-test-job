<?php

return [
    'class'    => 'yii\db\Connection',
    'dsn'      => 'mysql:host=localhost;dbname=main_db',
    'username' => 'yii-db-user',
    'password' => 'yii-db-pass',
    'charset'  => 'utf8',
];
