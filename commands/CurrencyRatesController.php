<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use app\models\Currency;


/**
 * This command for updating currency rates in the table "currency".
 *
 * @author Egor Kovyazin <egor.koviazin@gmail.com>
 * @since 1.0
 */
class CurrencyRatesController extends Controller
{
    /**
     * @param bool $verbose
     * @return int Exit code
     */
    public function actionUpdate($verbose = false)
    {
        $list = $this->getCurrencyRates();
        foreach ($list as $item) {
            $model = Currency::find()
                ->where('char_code = :char_code', ['char_code' => $item['CharCode']])
                ->one();
            
            if (empty($model)) {
                $model = new Currency();
            }

            $model->name      = $item['Name'];
            $model->rate      = $item['Value'] / $item['Nominal'];
            $model->char_code = $item['CharCode'];

            if ($model->save()) {
                $this->stdout(
                    sprintf('Saved currency: %d | %s | %s | %f', $model->id, $model->char_code, $model->name, $model->rate)
                    . PHP_EOL
                );
            }
        }

        return ExitCode::OK;
    }

    /**
     * @return array
     * 
     * @example:
     * [
     *      [CharCode] => JPY
     *      [Nominal]  => 100
     *      [Name]     => Японских иен
     *      [Value]    => 58,7907
     * ]
     */
    private function getCurrencyRates()
    {
        $xml = file_get_contents(Yii::$app->params['currencyRatesSourceUrl']);
        $xmlElement = simplexml_load_string($xml);

        if (empty($xmlElement->Valute)) {
            return [];
        }

        $list = [];
        foreach ($xmlElement->Valute as $node) {
            $node = (array)$node;

            if (isset($node['CharCode'], $node['Nominal'], $node['Name'], $node['Value'])) {
                $node['Value'] = (double)str_replace(',', '.', trim($node['Value']));
                $list[] = $node;
            }
        }

        return $list;
    }

}
