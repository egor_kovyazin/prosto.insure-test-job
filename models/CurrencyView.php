<?php

namespace app\models;

/**
 * ModelView for table "currency"
 *
 * @author Egor Kovyazin <egor.koviazin@gmail.com>
 * 
 * @property integer $id
 * @property string  $name
 * @property double  $rate
 */
class CurrencyView extends Currency
{
    public function fields()
    {
        return ['id', 'name', 'rate'];
    }
}
