<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Model for table "currency"
 *
 * @author Egor Kovyazin <egor.koviazin@gmail.com>
 * 
 * @property integer $id
 * @property string  $name
 * @property double  $rate
 * @property string  $char_code
 */
class Currency extends ActiveRecord
{
    public static function tableName()
    {
        return 'currency';
    }
}
