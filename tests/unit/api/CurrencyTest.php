<?php

namespace tests\unit\api;

class CurrencyTest extends \Codeception\Test\Unit
{
    const AUTH_TOKEN = '100-token';
    const URL_PREFIX = 'http://localhost/';

    public function testCurrenciesMethod()
    {
        $data = json_encode($this->getDataFromCurl('currencies'));

        expect(is_array($data));
    }

    public function testPaginationCurrenciesMethod()
    {
        $data = json_encode($this->getDataFromCurl('currencies/?per-page=2&page=2'));

        expect(is_array($data));
    }
    
    public function testCurrencyMethod()
    {
        $data = json_encode($this->getDataFromCurl('currency/2'));

        expect(is_array($data));
        expect(isset($data['rate']));
    }

        public function testFieldsCurrencyMethod()
        {
        $data = json_encode($this->getDataFromCurl('currency/2?fields=name,rate'));

        expect(is_array($data));
        expect(isset($data['rate']));
        expect(isset($data['id']));
    }

    private function getDataFromCurl($urlPostfix)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, self::URL_PREFIX . $urlPostfix);

        $headers = [
            'Accept: application/json',
            'Content-Type: application/json',
            'Authorization: Bearer ' . self::AUTH_TOKEN
        ];
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_HEADER, 0);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $out = curl_exec($curl);

        curl_close($curl);

        return $out;
    }

}
